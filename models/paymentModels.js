const logger = require('../logs')

const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../services/connectoDB');
const Address = require('./addressModel');
const ProductOrder = require('./productOrderModel');
/**
 * @swagger
 * definitions:
 *  payement:
 *      type: object
 *      properties:
 *          amount:
 *              type: float
 *          card_number:
 *              type: string
 *          validity_date:
 *              type: string
 *          ccv:
 *              type: string
 *          last_name:
 *              type: string
 *          first_name:
 *              type: string
 *          provider:
 *              type: string
 *          status:
 *              type: string
 */
const payment = sequelize.define('payment', {
        // Model attributes are defined here
        id: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true,
        },
        amount: {
            type: DataTypes.FLOAT,
            allowNull: false,
        },
        status: {
            type: DataTypes.STRING,
        }
    }, {}
);
module.exports = payment;