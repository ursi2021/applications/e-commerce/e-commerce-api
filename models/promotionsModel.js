const logger = require('../logs')

const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../services/connectoDB');
const promotionStore = require('./promotionStoreModel');
const promotionProduct = require('./promotionProductModel');

/**
 * @swagger
 * definitions:
 *  Promotion:
 *      type: object
 *      properties:
 *          start_date:
 *              type: Date
 *          end_date:
 *              type: Date
 *          offer_amount:
 *              type: float
 *          offer_percentage:
 *              type: float
 *          client_segment_id:
 *              type: int
 *          store_list:
 *              type: number // OTHER MODELS
 *          promotion_type:
 *              type: string
 *          minimum_amount:
 *              type: float
 *          description:
 *              type: string
 *          clients_id:
 *              type: string // Other models
 */
const promotion = sequelize.define('Promotion', {
        // Model attributes are defined here
        start_date: {
            type: DataTypes.DATE,
            allowNull: false
        },
        end_date: {
            type: DataTypes.DATE,
            allowNull: false
        },
        offer_percentage: {
            type: DataTypes.FLOAT,
            allowNull: true
        },
        offer_amount: {
            type: DataTypes.FLOAT,
            allowNull: true
        },
        client_segment_id: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        promotion_type: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: "web"
        },
        minimum_amount: {
            type: DataTypes.STRING,
            defaultValue: "0",
            allowNull: false,
        },
        description: {
            type: DataTypes.STRING,
            defaultValue: "0",
            allowNull: false,
        }
    }, {}
);

promotion.hasMany(promotionStore, { as: 'stores', onDelete: 'CASCADE', foreignKey: 'promotion_id'});
promotion.hasMany(promotionProduct, { as: 'products', onDelete: 'CASCADE', foreignKey: 'promotion_id'});
promotionProduct.belongsTo(promotion, { as: 'promotion', onDelete: 'CASCADE', foreignKey: 'promotion_id'});

module.exports = promotion;