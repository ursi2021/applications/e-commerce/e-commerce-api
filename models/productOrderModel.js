const logger = require('../logs')

const { Sequelize, DataTypes } = require('sequelize');
const Product = require('./productModel');
const sequelize = require('../services/connectoDB');
/**
 * @swagger
 * definitions:
 *  ProductOrder:
 *      type: object
 *      properties:
 *          price:
 *              type: float
 *          quantity:
 *              type: number
 */
const productOrder = sequelize.define('ProductOrder', {
        // Model attributes are defined here
        price: {
            type: DataTypes.FLOAT,
            allowNull: false,
        },
        quantity: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        promotion: {
            type: DataTypes.INTEGER,
            allowNull: true,
            defaultValue: undefined,
        }
    }, {}
);
productOrder.belongsTo(Product, {
    as: 'product',
    foreignKey: 'product_code',
    onDelete: 'CASCADE'})
module.exports = productOrder;