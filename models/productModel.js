const logger = require('../logs')

const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../services/connectoDB');

/**
 * @swagger
 * definitions:
 *  Product:
 *      type: object
 *      properties:
 *          product_code:
 *              type: string
 *          product_family:
 *              type: string
 *          product_description:
 *              type: string
 *          min_quantity:
 *              type: number
 *          packaging:
 *              type: number
 *          price:
 *              type: float
 *          assotiment_type:
 *              type: string
 */
const product = sequelize.define('Product', {
        // Model attributes are defined here
        product_code: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true
            // allowNull defaults to true
        },
        product_family: {
            type: DataTypes.STRING,
            allowNull: false
        },
        product_description: {
            type: DataTypes.STRING,
            allowNull: false
        },
        min_quantity: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        packaging: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        price: {
            type: DataTypes.FLOAT,
            allowNull: false,
        },
        assortment_type: {
            type: DataTypes.STRING,
            allowNull: false,
        }
    }, {}
);

module.exports = product;