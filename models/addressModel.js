const logger = require('../logs')

const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../services/connectoDB');

/**
 * @swagger
 * definitions:
 *  Address:
 *      type: object
 *      properties:
 *          street_number:
 *              type: string
 *          street:
 *              type: string
 *          city:
 *              type: string
 *          zipcode:
 *              type: string
 *          country:
 *              type: string
 */
const address = sequelize.define('Address', {
    // Model attributes are defined here
    street: {
        type: DataTypes.STRING,
        allowNull: false
    },
    street_number: {
        type: DataTypes.STRING,
        allowNull: false
        // allowNull defaults to true
    },
    city: {
        type: DataTypes.STRING,
        allowNull: false
    },
    zipcode: {
       type: DataTypes.STRING,
       allowNull: false
    },
    country: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {}
);
 module.exports = address;