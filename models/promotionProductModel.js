const logger = require('../logs')

const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../services/connectoDB');
const product = require('./productModel');

/**
 * @swagger
 * definitions:
 *  PromotionProduct:
 *      type: object
 *      properties:
 *          promotion_id:
 *              type: integer
 *          product_code:
 *              type: string
 */
const promotionProduct = sequelize.define('promotionProduct', {
        // Model attributes are defined here
    }, {}
);

promotionProduct.belongsTo(product, { as: 'product', onDelete: 'CASCADE', foreignKey: 'product_code'});
product.hasMany(promotionProduct, { as: 'promotions', onDelete: 'CASCADE', foreignKey: 'product_code'});

module.exports = promotionProduct;