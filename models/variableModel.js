const logger = require('../logs')

const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../services/connectoDB');

/**
 * @swagger
 * definitions:
 *  Variable:
 *      type: object
 *      properties:
 *          key:
 *              type: string
 *          value:
 *              type: integer
 */
const variable = sequelize.define('Variable', {
    // Model attributes are defined here
    key: {
        type: DataTypes.STRING,
        primaryKey: true,
    },
    value: {
        type: DataTypes.INTEGER,
        allowNull: false
        // allowNull defaults to true
    }
}, {}
);
 module.exports = variable;