const logger = require('../logs')

const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../services/connectoDB');
const Address = require('./addressModel');
const ProductOrder = require('./productOrderModel');
const Payement = require('./paymentModels');
/**
 * @swagger
 * definitions:
 *  Order:
 *      type: object
 *      properties:
 *          product_code:
 *              type: string
 *          price:
 *              type: float
 *          quantity:
 *              type: number
 *          day:
 *              type: number
 */
const order = sequelize.define('Order', {
        // Model attributes are defined here
        sum_price: {
            type: DataTypes.FLOAT,
            allowNull: false,
        },
        client_id: {
            type: DataTypes.STRING,
        },
        day: {
            type: DataTypes.INTEGER,
            allowNull: false,
        }
    }, {}
);

order.belongsTo(Address, {
    as: 'delivery_address',
    foreignKey: 'delivery_address_id',
});

order.belongsTo(Address, {
    as: 'invoice_address',
    foreignKey: 'invoice_address_id'
});

order.hasMany(ProductOrder, { as: 'products', onDelete: 'CASCADE'});
order.hasMany(Payement, { as: 'payments', onDelete: 'CASCADE'});
module.exports = order;