const Order = require('./orderModel');
const ProductOrder = require('./productOrderModel');
const Product = require('./productModel');
const Address = require('./addressModel');
const Payment = require('./paymentModels');
const Promotion = require('./promotionsModel');
const PromotionStore = require('./promotionStoreModel');
const PromotionProduct = require('./promotionProductModel');
const Variable = require('./variableModel');

const Models = {
    Address: Address,
    Order: Order,
    Product: Product,
    ProductOrder: ProductOrder,
    Payment: Payment,
    Promotion: Promotion,
    PromotionProduct: PromotionProduct,
    PromotionStore: PromotionStore,
    Variable: Variable,
};


//Sync All Models to database
async function dbsync() {
    for (const model in Models) {
        try {
            await Models[model].sync();
        } catch (e) {
            console.log(e);
        }
    }
    Variable.findOne({
        where: {
            key: 'day'
        }}).then((value) => {
            if (!value) {
            Variable.create({
                key: 'day',
                value: 1
            });
        }
        }).catch(() => {
            Variable.create({
                key: 'day',
                value: 1
            });
        })
}



dbsync().then().catch((e) => {console.log(e)});

module.exports = Models