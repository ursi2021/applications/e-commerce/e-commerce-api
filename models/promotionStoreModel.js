const logger = require('../logs')

const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../services/connectoDB');

/**
 * @swagger
 * definitions:
 *  PromotionStore:
 *      type: object
 *      properties:
 *          store:
 *              type: string
 *          promotionId:
 *              type: integer
 */
const promotionStore = sequelize.define('promotionStore', {
        // Model attributes are defined here
        store: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    }, {}
);

module.exports = promotionStore;