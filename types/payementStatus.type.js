module.exports = {
    ACCEPT : 'accepted',
    REJECT : 'declined',
    PENDING : 'pending',
}