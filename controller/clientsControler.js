const clientServices = require('../services/Clients');

function clientsControler(req, res, next) {
    clientServices.getClientList((clients) => {
        res.render('clients', { clients: clients });
    });
}

module.exports.clientsControler = clientsControler;