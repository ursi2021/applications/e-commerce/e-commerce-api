const orderService = require('../services/OrderService');
const productService = require('../services/productService');

function updateProductList(req, res, next) {
    productService.updateProductList().then((stats) => {
        res.status(200).json(stats);
    }).catch((e) => {
        res.status(500).json('Internal server error');
    })
}

function getProductList(req, res, next) {
    productService.getProductList().then((products) => {
        res.status(200).json(products);
    });
}

module.exports.updateProductList = updateProductList;
module.exports.getProductList = getProductList;
