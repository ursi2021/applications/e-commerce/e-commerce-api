const payementService = require('../services/PaymentService');

async function updatePaiement(req, res, next) {
    paymentService.updatePayement(req.data.id, req.data.status).then(() => {
        res.status(200).json("accepted");
    }).catch(() => {
        res.status(500).body('Internal Error');
    });
}

module.exports.addOrder = addOrder;