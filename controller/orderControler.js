const { Variable } = require('../models');
const orderService = require('../services/OrderService');
const paymentService = require('../services/PaymentService');

function getDayOrder(req, res, next) {
    if (req.query.order_id) {
        orderService.getOrder(req.query.order_id).then((order) => {
            if (order){
                res.status(200).json(order);
            } else {
                res.status(404).json('Order not found');
            }
        }).catch(
            res.status(500).json('Internal server error')
        )
    } else {
        orderService.getDayOrder().then((orders) => {
            result = orders.map((order) => ({
                    id: order.id,
                    date: order.createdAt,
                    'product-quantity-map': order.products.map((product) => ({
                        'product-code': product.product_code,
                        quantity: product.quantity,
                        price: product.price,
                    })),
                    'delivery-address': {
                        country: order.delivery_address.country, 
                        city: order.delivery_address.city, 
                        zipcode: order.delivery_address.zipcode,
                        street: order.delivery_address.street,
                        'stree-number' : order.delivery_address.street_number
                    },
                    'invoice-address': {
                        country: order.delivery_address.country,
                        city: order.delivery_address.city, 
                        zipcode: order.delivery_address.zipcode,
                        street: order.delivery_address.street,
                        'street-number' : order.delivery_address.street_number
                    },
                    'sum-price': order.sum_price,
                    'client-id': order.client_id,
                    'account' : order.client_id + '',
                })
            )
            res.status(200).json(result);
        })
    }
}

function addOrder(req, res, next) {
    let order = req.body;
    console.log(order);
    orderService.addOrder(order).then((isAdded) => {
        res.status(200).json({isSucess: isAdded});
    }).catch((e) => {
        console.log(e);
        res.status(500).json('Internal server error. ' )
    })
}

function getPayment(req, res, next) {
    paymentService.getPayments().then((payments) => {
        let result = payments.map((payment) => ({
            id: payment.id,
            amount: payment.amount,
            'card-number': payment.card_number,
            'validity-date': payment.validity_date,
            ccv: payment.cvc,
            'last-name': payment.last_name,
            'first-name': payment.first_name,
            provider: payment.provider,
        }));
        res.status(200).json(result[0]);
    });
}

async function getOrderForApp(req, res, next) {
    await paymentService.processPayements();
    orderService.getAllOrder().then((orders) => {
        res.status(200).json(orders);
    }).catch(() => {
        res.status(500).body('Internal Error');
    });
}

async function endDay(req, res, next) {
    const variable = await Variable.findOne({where: {
        key: 'day'
    }});
    Variable.update({
        value: variable.value + 1
    },{
        where: {
            key: 'day'
        }
    }).then((v) => {
        res.status(200).json(v);
    }).catch((e) => {
        res.status(500).json(e);
    })
}

async function viewOrder(req, res, next) {
    res.status(200).json(await orderService.getOrderInfo(req.query.id));
}

module.exports.addOrder = addOrder;
module.exports.getDayOrder = getDayOrder;
module.exports.getPayment = getPayment;
module.exports.getOrderForApp = getOrderForApp;
module.exports.endDay = endDay;
module.exports.viewOrder = viewOrder;