const simulator = require('../services/simulator.service');

async function makerOrder(req, res, next) {
    res.status(200).json(await simulator.createOrder());
}

function createClient(req, res, next) {
    res.status(200).json(simulator.createClient());
}

module.exports.makerOrder = makerOrder;
module.exports.createClient = createClient;