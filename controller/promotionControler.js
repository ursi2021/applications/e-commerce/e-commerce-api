const promotionService = require('../services/promotionService');

function updatePromotionList(req, res, next) {
    promotionService.updatePromotionList().then((stats) => {
        res.status(200).json(stats);
    }).catch((e) => {
        res.status(500).json('Internal server error');
    })
}

function getPromotionList(req, res, next) {
    promotionService.getPromotionList().then((products) => {
        res.status(200).json(products);
    });
}

module.exports.updatePromotionList = updatePromotionList;
module.exports.getPromotionList = getPromotionList;