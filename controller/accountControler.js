const logger = require('../logs');
const accountService = require('../services/accountService');
const refClientServcie = require('../services/referenciel-client.service');

function createOneAccount(req, res, next) {
    refClientServcie.createClient(req.body).then(() => {
        res.status(200).json();
    }).catch((e) => {
        logger.error(e);
        res.status(500).json();
    })
}

async function createMultipleAccount(req, res, next) {
    try {
    let result = {
        processed: 0,
        created: 0,
        conflicts: 0
    }
    let promises = []
    req.body.forEach(element => {
        promises.push(refClientServcie.createClient(element));
    });
    await Promise.all(promises).then((iscreateds) => {
        iscreateds.forEach((iscreated) => {
            result.processed += 1;
            if (iscreated) {
                result.created += 1;
            } else {
                result.conflicts += 1;
            }
        });
    });
    res.status(200).json(result);
} catch (e) {
    console.log(e);
    res.status(500).json("Internal error");
}
}

function getAll(req, res, next) {
    accountService.getAll().then((accounts) => {
        res.status(200).json(accounts)
    })
}

function getAllNew(req, res, next) {
    accountService.getAllNew().then((accounts) => {
        const result = accounts.map((value) => ({
            email: value.email,
            account: value.email,
            "first-name": value.firstName,
            "last-name": value.lastName,
            "civility" : value.civility,
            "address": value.address.map((address) => ({
                country: address.country,
                city: address.city,
                zipcode: address.zipcode,
                street: address.street,
                "street-number": address.street_number,
            })),
            "password": value.password,
            "created-at": value.createdAt,
            "updated-at": value.updatedAt,
        }))
        res.status(200).json(result);
    });
}

module.exports = {
    createMultipleAccount,
    createOneAccount,
    getAll,
    getAllNew,
}