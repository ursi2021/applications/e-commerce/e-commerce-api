const products = require('../models/exemple/products.json');
const address = require('../models/exemple/address.json');
const account = require('../models/exemple/account.json');

jest.mock('axios');

jest.mock('../services/connectoDB', () => {
  const Sequelize = require('sequelize');
  const sequelize = new Sequelize('database', 'username', 'password', {
    dialect: 'sqlite',
    storage: ':memory:',
    logging: false
  });
  sequelize.authenticate();
  return sequelize;
});

beforeAll(async () => {
  const Models = require('../models');
  for (const model in Models) {
    try {
        await Models[model].sync();
    } catch (e) {
        console.log(e);
    }
  }
  await Models.Account.bulkCreate(account);
  await Models.Address.bulkCreate(address);
  await Models.Product.bulkCreate(products);
})

const paiementService = require('../services/PaymentService');
const models = require('../models');

function createOrder(){ 
  return models.Order.create({
    sum_price: products[0].price + products[1].price,
    client_id: account[0].email,
    isSend: false,
    delivery_address_id: address[0].id,
    invoice_address_id: address[0].id,
  })
}

test('get payments', async () => {
  const order = await createOrder();
  const payment = await models.Payment.create({
    amount: products[0].price,
    card_number: '1234567890123456',
    first_name: account[0].firstName,
    last_name: account[0].lastName,
    cvc: '323',
    validity_date: '10-20',
    provider: 'mastercard',
    status: "pending",
    OrderId: order.id,
  });
  const payment2 = await models.Payment.create({
    amount: products[0].price,
    card_number: '1234567890123456',
    first_name: account[0].firstName,
    last_name: account[0].lastName,
    cvc: '323',
    validity_date: '10-20',
    provider: 'mastercard',
    status: "accepted",
    OrderId: order.id,
  });
  return paiementService.getPayments().then((list) => {
    expect(list.findIndex((value) => value.id === payment.id)).not.toBe(-1);
    expect(list.findIndex((value) => value.id === payment2.id)).toBe(-1);
  })
});

test('Processed payment accepted', async () => {
  const order = await createOrder();
  const axios = require('axios');
  axios.get.mockResolvedValue({ data: { status : 'accepted' }});
  const payment = await models.Payment.create({
    amount: products[0].price,
    card_number: '1234567890123456',
    first_name: account[0].firstName,
    last_name: account[0].lastName,
    cvc: '323',
    validity_date: '10-20',
    provider: 'mastercard',
    status: "pending",
    OrderId: order.id,
  });
  await paiementService.processPayements();
  return paiementService.getPayments().then((list) => {
    expect(list.findIndex((value) => value.id === payment.id)).toBe(-1);
  })
})

test('Processed payment not found', async () => {
  const order = await createOrder();
  const axios = require('axios');
  axios.get.mockResolvedValue();
  const payment = await models.Payment.create({
    amount: products[0].price,
    card_number: '1234567890123456',
    first_name: account[0].firstName,
    last_name: account[0].lastName,
    cvc: '323',
    validity_date: '10-20',
    provider: 'mastercard',
    status: "pending",
    OrderId: order.id,
  });
  await paiementService.processPayements();
  return paiementService.getPayments().then((list) => {
    expect(list.findIndex((value) => value.id === payment.id)).not.toBe(-1);
  })
});

test('Processed payment axios error', async () => {
  const order = await createOrder();
  const axios = require('axios');
  axios.get.mockRejectedValue(new Error('Async error'));;
  const payment = await models.Payment.create({
    amount: products[0].price,
    card_number: '1234567890123456',
    first_name: account[0].firstName,
    last_name: account[0].lastName,
    cvc: '323',
    validity_date: '10-20',
    provider: 'mastercard',
    status: "pending",
    OrderId: order.id,
  });
  await paiementService.processPayements();
  return paiementService.getPayments().then((list) => {
    expect(list.findIndex((value) => value.id === payment.id)).not.toBe(-1);
  })
})