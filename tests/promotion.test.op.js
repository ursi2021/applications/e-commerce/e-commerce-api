jest.mock('axios');

jest.mock('../services/connectoDB', () => {
  const Sequelize = require('sequelize');
  const sequelize = new Sequelize('database', 'username', 'password', {
    dialect: 'sqlite',
    storage: ':memory:',
    logging: false
  });
  sequelize.authenticate();
  return sequelize;
});

const promotion = require('../services/promotionService');

test('SimpleTest', () => {
    return expect(10).toBe(10);
});