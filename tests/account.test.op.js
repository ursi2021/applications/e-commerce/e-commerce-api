/*var chai = require('chai');
chai.use(require('chai-http'));
const app = require("../app");
var assert = require('assert');
const { doesNotMatch } = require('assert');
const { expect } = chai;*/

const axios = require('axios');
const client = require('../services/Clients');

jest.mock('axios');

jest.mock('../services/connectoDB', () => {
  const Sequelize = require('sequelize');
  const sequelize = new Sequelize('database', 'username', 'password', {
    dialect: 'sqlite',
    storage: ':memory:',
    logging: false
  });
  sequelize.authenticate();
  return sequelize;
});

const account = require('../services/accountService');

function getNewAccount(email) {
  return {
    "firstName": "Dupont",
    "lastName": "Dupont",
    "email": email,
    "password": "azerty",
    "civility": "homme",
    "address" : [
      {
          "street": "Rue Rivoli",
          "street_number": "1",
          "city" : "Paris",
          "zipcode" : "75001",
          "country" : "france"
      }
  ]
}
}

beforeEach(async () => {
  const Models = require('../models');
  for (const model in Models) {
    try {
        await Models[model].sync();
    } catch (e) {
        console.log(e);
    }
}
});

test('Get client list ( deprecated) ', () => {
  axios.get.mockResolvedValue({ data: ['a', 'b', 'c'], status : 200 });
  return client.getClientList(data => expect(data).toEqual(['a', 'b', 'c']));
}); 

test('Create one account', () => {
  
  const newAccount = getNewAccount('account1@ursi.fr');
  return account.createOne(newAccount).then((accepted) => { expect(accepted).toBe(true)})
})

test('Can\'t create same account', () => {
  const newAccount = getNewAccount('account2@ursi.fr')
return account.createOne(newAccount).then((accepted) => {
  expect(accepted).toBe(true);
  return account.createOne(newAccount).then((reject) => { 
    return expect(reject).toBe(false);
    });
  });
} )

test('Get all account', () => {
  const newAccount = getNewAccount('account3@ursi.fr')
  return account.createOne(newAccount).then((accepted) => { 
    expect(accepted).toBe(true);
    return account.getAll().then((res) => {
      const index = res.findIndex((value) => value.email === newAccount.email);
      return expect(index).not.toBe(-1);
    });
  });
})

test('Get all new account', () => {
  const newAccount = getNewAccount('account4@ursi.fr')
  return account.createOne(newAccount).then((accepted) => { 
    expect(accepted).toBe(true);
    return account.getAllNew().then((newAccountList) => {
      const index = newAccountList.findIndex((val) => val.email === newAccount.email);
      expect(index).not.toBe(-1);
      return account.getAllNew().then((umptyList) => {
        const index = umptyList.findIndex((val) => val.email === newAccount.email);
        return expect(index).toBe(-1);
      })
    })
  });
})