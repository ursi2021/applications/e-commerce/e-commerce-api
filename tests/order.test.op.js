jest.mock('axios');

const products = require('../models/exemple/products.json');
const address = require('../models/exemple/address.json');
const account = require('../models/exemple/account.json');

jest.mock('../services/connectoDB', () => {
  const Sequelize = require('sequelize');
  const sequelize = new Sequelize('database', 'username', 'password', {
    dialect: 'sqlite',
    storage: ':memory:',
    logging: false
  });
  sequelize.authenticate();
  return sequelize;
});

beforeAll(async () => {
  const Models = require('../models');
  for (const model in Models) {
    try {
        await Models[model].sync();
    } catch (e) {
        console.log(e);
    }
  }
  await Models.Account.bulkCreate(account);
  await Models.Address.bulkCreate(address);
  await Models.Product.bulkCreate(products);
})

const orderService = require('../services/OrderService');
const models = require('../models');
const order = require('../models/orderModel');

test('Get order', async () => {
  const order = await models.Order.create({
    sum_price: products[0].price + products[1].price,
    client_id: account[0].email,
    isSend: false,
    delivery_address_id: address[0].id,
    invoice_address_id: address[0].id,
  })
  return orderService.getOrder(order.id).then((orderGeted) => {
    return expect(orderGeted.sum_price).toBe(products[0].price + products[1].price)
  })
})

test('Get not found order', async () => {
  return expect(orderService.getOrder(12345)).resolves.toBe(null);
})

test('Get All Order', async () => {
  const orderA = await models.Order.create({
    sum_price: products[0].price,
    client_id: account[0].email,
    isSend: false,
    delivery_address_id: address[0].id,
    invoice_address_id: address[0].id,
  });
  const orderProd = await models.ProductOrder.create({
    product_code: products[0].product_code,
    quantity: 1,
    price: products[0].price,
    OrderId: orderA.id,
  });
  const payment = await models.Payment.create({
    amount: products[0].price,
    card_number: '1234567890123456',
    first_name: account[0].firstName,
    last_name: account[0].lastName,
    cvc: '323',
    validity_date: '10-20',
    provider: 'mastercard',
    status: "pending",
    OrderId: orderA.id,
  });
  const orderList = await orderService.getAllOrder();
  const id = orderList.findIndex((value) => value.id === orderA.id)
  expect(id).not.toBe(-1);
  expect(orderList[id].products[0].id).toBe(orderProd.id);
  expect(orderList[id].payments[0].id).toBe(payment.id);
})


test('add new order', async () => {
  const newOrder = {
    sum_price: products[0].price,
    client_id: account[0].email,
    isSend: false,
    delivery_address_id: address[0].id,
    invoice_address_id: address[0].id,
    credit_card : {
      card_number: '1234567890123456',
      first_name: account[0].firstName,
      last_name: account[0].lastName,
      cvc: '323',
      validity_date: '10-20',
      provider: 'mastercard'
    },
    products : [{
      product_code: products[0].product_code,
      quantity: 1,
      price: products[0].price
    }]
  }
  return expect(orderService.addOrder(newOrder)).resolves.toBe(true);
})

test('imposible new order', () => {
  return expect(orderService.addOrder({})).resolves.toBe(false);
})