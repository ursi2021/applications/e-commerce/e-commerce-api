# Call a E commerce 

## Les Sorties de l'application 

### Mettre  a disposition les ventes du jour (Presque ok)

- Route : /e-commerce/web-sales
- Methode : GET
- Remarque : pour le moement toute les vente sont mise a disposition et ne sont pas triée ( paramètre a rajouté a la query )



## Les Entrées de l'application

### Recevoir les infos des nouveaux client (OK)

- Route :
  - /e-commerce/account/create/one ( creer un compte)
  - /e-commerce/account/create/multiple ( creer plusieur compte )
- Methode : POST
- Remarque OK



### Recevoir la validation de paiement (OK mais pas testé)

- route : e-commerce/paiement/update

- Methode : POST

- Format de la requette : 

  ```json
  {
      id: String //id of payement
      status: ['accept', 'reject', 'pending'] //Status of payement
  }
  ```

- Reponse : Status 200 ou 500

- Remarque : c'est moi qui appelais monetique et paiement, il  y a un changement de sens a opéré



### Recevoir les commandes client (Intégrer la récupération du compte client et Bancaire )

- Route : /e-commerce/order/add

- Methode : POST

- format : 

  ```json
  {
  	account : account,
      products : {
          "product-code" : string,
          "quantity" : number,
      }[],
      "delivery-address": {
          'street' : string,
          'number' : string,
          'city' : string,
          'zipcode': string,
          'country': string,
      } | number, 
      "billing-address" : {
          'street' : string,
          'number' : string,
          'city' : string,
          'zipcode': string,
          'country': string,
      } | number,
      card : {
          'ccv' : number,
          'number' : string,
          'validity-date': 'MM/AA'
      } //Optionnel
      payement-methode : ['credit-card', 'internal'] // Actuellement optionnel
  }
  ```

  

- Remarque changement à opérer dans le format d'entrée

- Liste des etapes :

  - Récuperer l'identifiant de l'utilisateur 
  - Verifier qu'il existe //Pas fait 
  - Verifier qu'on a les produit en stock  // Pas fait
  - Appliquer les promotion applicable a la commande  //Pas fait 
  - Enregistrer dans la BDD la commande //OK
  - Si mode de paiement choisi c'est CB //OK mais voir avec monetique et paiement.
    - Récupérer les information bancaire par rapport a la personne et sont compte
    - Verifier que c'est les bonnes 
    - Enregistrer le paiement dans la BDD
    - Les envoyer en demande de paiement à monetique et payement ( attendre le retour plus tard)
  - Si mode paiement choisi c'est interne //Pas a faire encore mais preparé
    - Demander le solde du compte interne 
    - Enregistrer le paiement dans la BDD
    - Effectuer une demande de paiement au compte 
  - Envoyer 200 si tout est bon sinon erreur 



# Les call aux autre application

### Récuprérer les promotions (OK)

- **Déclancheur : (clock) Requette GET sur l'application**

  - Route déclancheur  : clock (/e-commerce/promotion/load)
  - Methode : GET

- Route appelé : /gestion-promotion/offers

- Methode route appelé : GET

- Format attendu :

  ```
  {
  	id: number,
  	start-date: date,
  	end-date: date,
  	offer-percentage: number,
  	...
  }
  ```

  

### Récupérer l'assortiment (OK)

- **Déclancheur : (Clock) Requette GET sur l'application**

  - Route déclancheur : /e-commerce/product/load
  - Methode : GET

- Route appelé : referenciel-produit/products/web

- Methode route appelé : GET

- Format attendu : 

  ```
  {
  ...
  }
  ```

  

- Remarque :  Je nais pas les stocks avec 



### Regrouper les ventes du jour 

- **Déclancheur : Clock, Requette GET sur l'application**
  - Route : /e-commerce/order/end-day
  - Methode : GET
- Route appelé : Processus interne

### Recuperer les information bancaire d'un client ( Pas fait ) ( contacter relation client ) 

- **Déclancheur : quand on reçois une nouvelle commande, on récupère les information bancaire pour les envoyer à monetique et paiement**

- Route appelé : /client-bank-details

- Methode : GET

- Paramètre à envoyer : account

- Service : Relation Client **(Route Non fonctionnelle, pas de retour)**

- Retour souhaité (source Excel 05/01/2021) :

  ```
  {
  	id: Number,
  	card-number: Number,
  	validity-date: Date,
  	ccv: Number
  }
  ```

  

- Remarque : non fait 



### Recupérer les infos de login ( fait, pas tester, contacter realtion client  )

- **Déclancheur : Quand on recoit une commande pour verifier sont propriétaire.**

- Route à appeler : /account  **(Il n'y a que une route pour tout récupérer, est ce que ça ve dire que je recuper tout a chaque fois ?)**

- Methode : GET

- Paramettre a donner : account

- Service : Relation client 

- Retour souhaité ( source Excel 05/01/2021 ) :

  ```json
  {
  	account: String,
  	email: String,
  	password: String
  }
  ```

- Remarque : il n'y aurait pas des creds a passer avec ? 



### Envoyer les ventes web à Relation client et gestion entrepôt ( fait pour gestion entrepot )

- **Déclancheur : Quand le paiement de la commande est validée ( en mode automatique )**

- Route à appeler : 

  - Relation Client : **Non présente dans la liste des route de l'application**
  - Gestion entrepôt : **/gestion-entrepots/data/web-sales** 

- Methode à appeler  : POST

- Format envoyé ( body ): 

  ```json
  {
  	id: Number,
  	date : String( format JJ-MM-AA hh:mm:ss)
      product-quantity-map: {
      	product-code: String,
      	quantity: Number
  	}[],
  	account: String,
  	delivery-address: {country : string , city : string , zipcode : int , street : string , street-number : int},
  	invoice-address: {country : string , city : string , zipcode : int , street : string , street-number : int},
  	sum-price: Float
  }
  ```

- Retour attendu : ResponceCode : 200 ( pas de data)  500 si erreur 




### Envoyer les informations des nouveaux client ( OK, Pas testé )

- **Déclancheur : Quand on reçois des informations de nouveaux client ( rôle proxy )**

- route a appeler : /relation-client/new-account

- Methode à appelé : POST 

- Service : Relation client 

- Format envoyé :

  ```json
  {
      email: string,
      "first-name": string,
      "last-name": account.lastName,
      "civility" : account.civility,
      "address": {
          country: string,
          city: string,
          zipcode: string,
          street: string,
          "street-number": string,
      }[],
      "cards": {
          ccv: number,
          "card-number": string,
          "validity-date" : String (DD/AA),
          "provider" : String
      }[],
      "password": string, //Non chiffré
  }
  ```

- Retour : Responce code 200



### Envoyer une demande de paiement ( OK, pas testé )

- **Déclancheur : Quand j'ai récupéré les informations bancaire**

- route appelé : **/monetique-paiement/payment**

- Format data : 

  ```json
  {
  	from: 'e-commerce',
      card: {
          "cardNumber": String,
        	"validityDate": Date ( en string ),
        	"CCV": String,
        	"lastName": String,
        	"firstName": String,
          "Provider": String
      },
      amount: amount
  }
  ```

  

- Methode : POST

- Service : Monetique et paiement

- Remarque c'était monetique qui recupérer et après mois qui venait chercher ( il y a un inversement de sens )

- Dans le process Monetique et paiement devrai m'envoyer le 

- Retour attendu : Code 200

  ```json
  {
      id: String
  }
  ```

  





# Remarque en sup 

- Code coverage : 58% des services
- Necessite une refonte des process dans le back ( et pas mal de suppression )

