FROM nikolaik/python-nodejs

WORKDIR /usr/src/app

COPY package*.json ./

RUN apt-get -q update
RUN apt-get -qy install netcat
RUN npm install

COPY . .

CMD [ "node", "bin/www" ]
