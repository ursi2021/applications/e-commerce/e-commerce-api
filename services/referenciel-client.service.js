const { default: Axios } = require("axios");
//const { expect } = require("chai");
const logger = require("../logs");

function getClient (login) {
    return Axios.get(process.env.URL_RELATION_CLIENT + '/client-account/' + login).then((resp) => {
        console.log(resp.data);
        if (resp.data[0] && resp.data[0].account) {
            return resp.data[0];
        } else {
            throw 'invalide user recieved.'
        }
    }).catch((e) => {
        logger.error(e);
        throw 'Request error.'
    })
}

async function checkLogin (login, password) {
    try {
        const client = await getClient(login);
        if (client.password === password) {
            return client;
        } else {
            return false;
        }
    } catch (e) {
        return false;
    }
}

function getClientCB(account, cardId = -1) {
    if (account && account.account && account.password) {
        return Axios.get(process.env.URL_RELATION_CLIENT + '/bank-details/' + account.account).then((resp) => {
            if ( cardId != -1) {
                const place =resp.data.findIndex((value) => value.id === cardId);
                if (place != -1) {
                    return resp.data[place];
                } else {
                    throw 'cb not found';
                }
            }
            return resp.data;
        }).catch((e) => {
            throw 'Request error';
        });
    } else {
        throw 'bad login';
    }
}

function createClient(account) {
    logger.info('NEW ACCOUNT => ' + JSON.stringify(account))
    return Axios.post(process.env.URL_RELATION_CLIENT + '/new-account', [account]);
}

module.exports.getClient = getClient;
module.exports.checkLogin = checkLogin;
module.exports.getClientCB = getClientCB;
module.exports.createClient = createClient;