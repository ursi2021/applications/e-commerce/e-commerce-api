const models = require('../models');
const axios = require('axios');
const logger = require('../logs');

async function updateProductList(){
    //models.Product.findAll();
    let updated = 0;
    let created = 0;
    let total = 0;
    const list = await axios.get(process.env.URL_REFERENTIEL_PRODUIT + '/products/web').then((resp) => {
        logger.info(resp);
        if (resp.status === 200){
            resp.data.forEach((value) => {
                total++;
                models.Product.upsert({
                    product_code: value['product-code'],
                    product_family: value['product-family'],
                    product_description: value['product-description'],
                    min_quantity: value['min-quantity'],
                    packaging: value.packaging,
                    price: value.price,
                    assortment_type: value['assortment-type'],
                }).then(function(test){
                    console.log(test[1]);
                    if (test[1]) {
                        created++;
                    } else {
                        updated++;
                    }
                    //test returned here as true or false how can i get the inserted id here so i can insert data in other tables using this new id?
                })
            });
            return true;
        }
        return false;
    }).catch((e) => {
        logger.error(e);
        return false;
    })
    if (list){
        return {updated, created, total};
    } else {
        return false
    }
}

async function getProduct(){
    const res = await models.Product.findAll({
        include: [
            {
                model: models.PromotionProduct,
                as: 'promotions',
                foreignKey: 'product_code',
                include: [{
                    model: models.Promotion,
                    as: 'promotion',
                    /*where: {
                        start_date: {
                            $lt: Date.now(),
                        },
                        end_date: {
                            $gt: Date.now(),
                        }
                    }*/
                }]
            }]
    });
    return res;
}

module.exports.updateProductList = updateProductList;
module.exports.getProductList = getProduct;