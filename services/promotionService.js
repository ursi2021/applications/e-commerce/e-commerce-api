const models = require('../models');
const axios = require('axios');
const moment = require('moment');

async function updatePromotionList() {
    //models.Product.findAll();
    console.log('UPDATE PROMO');
    let updated = 0;
    let created = 0;
    let total = 0;
    const list = await axios.get(process.env.URL_GESTION_PROMOTION + '/offers').then(async (resp) => {
        if (resp.status === 200) {
            console.log(resp.data);
            if (resp.data)
            Object.values(resp.data).forEach((value) => {
                total++;
                        if (!value.id) { return ;}
                        models.Promotion.upsert({
                            id: value.id,
                            start_date: moment(value['start-date'], 'DD-MM-YYYY hh:mm:ss'),
                            end_date: moment(value['end-date'], 'DD-MM-YYYY hh:mm:ss'),
                            offer_percentage: value['offer-percentage'],
                            offer_amount: value['offer_amount'],
                            client_segment_id: value['client-segment-id'],
                            promotion_type: value['promotion-type'],
                            minimum_amount: value['minimum-amount'],
                            description: value.description,
                            client_id: value['client-id'],
                        }).then(async function (promotion) {
                            if (promotion) {
                                await models.PromotionStore.destroy({
                                    where: {
                                        promotion_id: value.id,
                                    }
                                });
                                await models.PromotionProduct.destroy({
                                    where: {
                                        promotion_id: value.id,
                                    }
                                });
                                value['stores-list'].forEach((store) => {
                                    models.PromotionStore.create({
                                        promotion_id: value.id,
                                        store: store,
                                    }).then();
                                });
                                value['products-list'].forEach((product) => {
                                    models.PromotionProduct.create({
                                        promotion_id: value.id,
                                        product_code: product,
                                    }).then();
                                })
                            }
                        });
                });
            return true;
        }
        return false;
    }).catch((e) => {
        return false;
    });
    if (list) {
        return { updated, created, total };
    }
    return 'false'
}

async function getPromotionList() {
    const res = await models.Promotion.findAll({
        include: ['stores', 'products'],
        limit: 2,
        order: [['id', 'DESC']]
    });
    return res;
}

function getfirstPromo(product) {
    console.log(product);
    for (let promo in product.promotions) {
        if (product.promotions[promo].promotion) {
            const promotion = product.promotions[promo].promotion;
            const start = new Date(promotion.start_date);
            const end = new Date(promotion.end_date);
            if (start < Date.now() && end > Date.now()) { // Need check minimum amount 
                return promotion;
            }
        }
    }
    return undefined;
}


function applyPromotionIntern (product, promotion) {
    if(promotion && promotion.offer_percentage !== null) {
        return (product.price -  (product.price * promotion.offer_percentage / 100)).toFixed(2);
    }
    if (promotion && promotion.offer_amount !== null) {
        return (product.price - promotion.offer_amount).toFixed(2);
    }
    return product.price;
}


function findPromotion(promotion_list, product_code, sum_price, product) {
    const promotion = getfirstPromo(product);
    const price = applyPromotionIntern(product, promotion);
    return {
        price: price,
        promotion_id: promotion ? promotion.id : undefined,
    }
}

async function applyPromotion(orderList, sum_price) {
    let result = [];
    for (const i in orderList) {
        const product = await models.Product.findOne({
            where: {
                product_code : orderList[i]['product-code']
            },
            include: [
                {
                    model: models.PromotionProduct,
                    as: 'promotions',
                    foreignKey: 'product_code',
                    include: [{
                        model: models.Promotion,
                        as: 'promotion',
                        /*where: {
                            start_date: {
                                $lt: Date.now(),
                            },
                            end_date: {
                                $gt: Date.now(),
                            }
                        }*/
                    }]
                }]
        });
        const new_price = findPromotion(product.promotion, orderList[i].product_code, sum_price, product);
        result.push({
            ...orderList[i],
            price: new_price.price,
            promotion: new_price.promotion_id,
        });
    }
    return result;
}

module.exports.updatePromotionList = updatePromotionList;
module.exports.getPromotionList = getPromotionList;
module.exports.applyPromotion = applyPromotion;