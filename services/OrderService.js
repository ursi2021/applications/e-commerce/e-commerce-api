const { Order, ProductOrder, Address, Payment, PromotionProduct, Promotion, Variable } = require('../models')
const refClientService = require('./referenciel-client.service');
const payementService = require('./PaymentService');
const promotionService = require('./promotionService');
const product = require('../models/productModel');
const statusType = require('../types/payementStatus.type');
const logger = require('../logs');

async function getOrder(orderId) {
    const order = await Order.findOne({
        where: { id: orderId }
    });
    return order;
}

async function getOrderInfo(orderId) {
    const order = await Order.findOne({
        where: { id: orderId},
        include: [
            {
                model: Address,
                as: 'delivery_address'
            },
            {
                model: Address,
                as: 'invoice_address'
            },
            {
                model: ProductOrder,
                as: 'products',
            },
            {
                model: Payment,
                as: 'payments',
            }]
    });
    return order
}

async function getDayOrder() {
    const day = await Variable.findOne({where: {key: 'day'}});
    const orders = await Order.findAll({
        where: {
            day: day.value - 1
        },
        include: [
            {
                model: Address,
                as: 'delivery_address'
            },
            {
                model: Address,
                as: 'invoice_address'
            },
            {
                model: ProductOrder,
                as: 'products',
            },
            {
                model: Payment,
                as: 'payments',
                where: {
                    status: statusType.ACCEPT
                }
            }]
    });
    return orders;
}

async function getAllOrder() {
    const orders = await Order.findAll({
        include: [
            {
                model: Address,
                as: 'delivery_address'
            },
            {
                model: Address,
                as: 'invoice_address'
            },
            {
                model: ProductOrder,
                as: 'products',
            },
            {
                model: Payment,
                as: 'payments',
            }]
    });
    return orders;
}

async function addOrder(order) {
    try {
        logger.info('New Order');
        logger.info(order);
        const account = await refClientService.getClient(order.account);
        logger.info('Order client ' + account.account )
        const productsCommand = order.products; //Verifier le stock (remplacer cette ligne)
        let sum_price_unpromoted = 0;
        productsCommand.forEach(element => {
            sum_price_unpromoted += element.price * element.quantity;
        });

        const productsPromoted = await promotionService.applyPromotion(productsCommand, sum_price_unpromoted); //Appliquer les pormotions disponible
        logger.info(productsPromoted);
        let sum_price = 0;
        productsPromoted.forEach(element => {
            sum_price += element.price * element.quantity;
        });
        const day = await Variable.findOne({
            where: {
                key: 'day'
            }
        })
        const delive_adress = await Address.create({
            ...order["delivery-address"],
            street_number: order["delivery-address"]["street_number"]
        });
        const billing_adress = await Address.create({
            ...order["billing-address"],
            street_number: order["billing-address"]["street_number"]
        });
        const newOrder = await Order.create({
            sum_price: sum_price,
            client_id: account.account,
            day: day.value, 
            delivery_address_id: delive_adress.id,
            invoice_address_id: billing_adress.id,
        });
        //console.log ( newOrder);
        for (const i in productsPromoted) {
            //console.log(product);
            ProductOrder.create({ ...productsPromoted[i], OrderId: newOrder.id, product_code: productsPromoted[i]['product-code'] }).then().catch(e => logger.error(e));
        }
        if (order['payement-methode'] === 'credit-card' || order['payement-methode'] === undefined) {
            await payementService.processCBPayement(account, newOrder.id, sum_price, order.card);
        } else {
            await payementService.processInternalPayement(account, newOrder.id, sum_price);
        }
        return true;
    } catch (e) {
        logger.error(e);
        return false;
    }
};

module.exports.getOrder = getOrder;
module.exports.getAllOrder = getAllOrder;
module.exports.getDayOrder = getDayOrder;
module.exports.addOrder = addOrder;
module.exports.getOrderInfo = getOrderInfo;