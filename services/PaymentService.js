const { default: Axios } = require('axios');
const { Payment, Order } = require('../models');
const OrderService = require('./OrderService');
const axios = require('axios');
const logger = require('../logs');
const payementStatus = require('../types/payementStatus.type');
const refClient = require('./referenciel-client.service');

async function getPayments() {
    const payments = await Payment.findAll({
        where: {
            status: 'pending',
    }});
    return payments;
}

async function getOnePayment(id) {
    return await Payment.findOne({
        where: {
            id: id
        }
    })
}

async function updatePayement(paymentId, status) {
    if (status !== payementStatus.ACCEPT && status !== payementStatus.PENDING && status !== payementStatus.REJECT) {
        throw 'Invalide status parameter.'
    }
    if (status === payementStatus.ACCEPT)
    return Payment.update({
        status : status
    },{
        where : {
            id: paymentId
        }
    }).then(async (rowUpdated) => {
        if (rowUpdated === 0) {
            throw 'Payement reference not found.';
        } else {
            if (status === payementStatus.ACCEPT) {
                const payment = await getOnePayment(paymentId);
                const command = await OrderService.getOrderInfo(payment.orderId);
                const info = {
                    id: command.id,
                    date : command.date,
                    "product-quantity-map": command.products.map((value) => { return {
                        "product-code": value.product_code,
                        quantity: value.quantity,
                    }}),
                    account: command.client_id,
                    "delivery-address": {
                        country : command.delivery_address.country , 
                        city : command.delivery_address.city , 
                        zipcode : command.delivery_address.zipcode , 
                        street : command.delivery_address.street , 
                        "street-number" : command.delivery_address.street_number},
                    "invoice-address": {
                        country : command.invoice_address.country , 
                        city : command.invoice_address.city , 
                        zipcode : command.invoice_address.zipcode , 
                        street : command.invoice_address.street , 
                        "street-number" : command.invoice_address.street_number},
                    "sum-price": command.sum_price,
                }
                axios.post(process.env.URL_ENTREPROTS + "/data/web-sales", info).then().catch();
                axios.post(process.env.URL_RELATION_CLIENT + "/relation-client/data/web-sales", info).then().catch();
            }
        }
    }).catch(() => {
        throw 'Internal sequelize error.';
    });
}

async function processInternalPayement(account, order, amount) {
    throw 'Not implemented'
}

async function processCBPayement(account, orderId, amount) {
    logger.info('ORDER ID = ' + orderId);
    const request = await Axios.post(process.env.URL_MONETIQUE_PAIEMENT + '/payment', {
        from: 'e-commerce',
        account: account,
        amount: amount
    });
    const payment = await Payment.create({
        id: request.data.id,
        amount: amount,
        status: request.data.status,
        OrderId: orderId,
    });
    if (request.data.status === payementStatus.ACCEPT) {
        const command = await OrderService.getOrderInfo(orderId);
        const info = {
            id: command.id,
            date : command.date,
            "product-quantity-map": command.products.map((value) => { return {
                "product-code": value.product_code,
                quantity: value.quantity,
            }}),
            account: command.client_id,
            "delivery-address": {
                country : command.delivery_address.country , 
                city : command.delivery_address.city , 
                zipcode : command.delivery_address.zipcode , 
                street : command.delivery_address.street , 
                "street-number" : command.delivery_address.street_number},
            "invoice-address": {
                country : command.invoice_address.country , 
                city : command.invoice_address.city , 
                zipcode : command.invoice_address.zipcode , 
                street : command.invoice_address.street , 
                "street-number" : command.invoice_address.street_number},
            "sum-price": command.sum_price,
        }
        logger.info('data send to entrepot and relation client : ' + JSON.stringify(info));
        axios.post(process.env.URL_ENTREPROTS + "/data/web-sales", info).then((r) => {
            logger.info('Send info to gestion entrepot, status : ' + r.status)
        }).catch((e) => {
            logger.error('Cannot send order to gestion entrepot : ' + e)
        });
        axios.post(process.env.URL_RELATION_CLIENT + "/data/web-sales", info).then((r) => {
            logger.info('Send info to relation client, status : ' + r.status)
        }).catch((e) => {
            logger.error('Cannot send order to reletion client  : ' + e)
        });
    }
}

module.exports.processInternalPayement = processInternalPayement;
module.exports.processCBPayement = processCBPayement;
module.exports.getPayments = getPayments;
module.exports.updatePayement = updatePayement;

//================== DEPRECATED ZONE ======================

//Deprecated
async function processPayements() {
    const paiements = await Payment.findAll({
        where: {
            status: 'pending',
        }
    })
    let waitingList = [];
    paiements.forEach(paiement =>{
        waitingList.push(getResultPayement(paiement));
    });
    await Promise.all(waitingList);
}

// Deprecated
async function getResultPayement(payment) {
    return await axios.get(process.env.URL_MONETIQUE_PAIEMENT +  '/payments/' + payment.id + '?from=e-commerce', {
        timeout: 500
    }).then((res) => {
        logger.info('Update payement : ' + payment.id);
        if (res && res.data) {
            payment.status = res.data.status;
            payment.save();
            return res.data.status;
        } else {
            return "pending"
        }
    }).catch((e) => { 
        logger.error('fail to call monetique and payement for update payement. Reason : ' + e);
        return "pending"
    });
}


module.exports.processPayements = processPayements;