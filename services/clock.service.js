const hour = [8,9,10,11,12,13,14,15,16,17,18,19,20]
const min = [30,0, 15, 45]

let res = []
for (const i in hour) {
    for (const y in min) {
        res.push({
            j: 0, //index for day of the week. Monday at 1, Sunday at 7, 0 is everyday, null for specific date
            h: hour[i], // required, must be valid
            m: min[y], // required, must be valid
            date: null, // Date format if j != null, else  null.
            name: "Creer une vente random h: " + hour[i] + " m: " + min[y]  , // required
            route: "/e-commerce/sim/make-order", // required
        });
    }
}


module.exports = 
[{
    j: 0, //index for day of the week. Monday at 1, Sunday at 7, 0 is everyday, null for specific date
    h: 7, // required, must be valid
    m: 40, // required, must be valid
    date: null, // Date format if j != null, else  null.
    name: "Récupérer les promotions", // required
    route: "/e-commerce/promotion/load", // required
},
{
    j: 0, //index for day of the week. Monday at 1, Sunday at 7, 0 is everyday, null for specific date
    h: 7, // required, must be valid
    m: 40, // required, must be valid
    date: null, // Date format if j != null, else  null.
    name: "Récupérer l'assortiment Web", // required
    route: "/e-commerce/product/load", // required
},
{
    j: 0, //index for day of the week. Monday at 1, Sunday at 7, 0 is everyday, null for specific date
    h: 20, // required, must be valid
    m: 30, // required, must be valid
    date: null, // Date format if j != null, else  null.
    name: "Regrouper les ventes du jour", // required
    route: "/e-commerce/order/end-day", // required
},
{
    j: 0, //index for day of the week. Monday at 1, Sunday at 7, 0 is everyday, null for specific date
    h: 15, // required, must be valid
    m: 30, // required, must be valid
    date: null, // Date format if j != null, else  null.
    name: "Creer un compte random", // required
    route: "/e-commerce/sim/create-client", // required
},
{
    j: 0, //index for day of the week. Monday at 1, Sunday at 7, 0 is everyday, null for specific date
    h: 17, // required, must be valid
    m: 30, // required, must be valid
    date: null, // Date format if j != null, else  null.
    name: "Creer un compte random", // required
    route: "/e-commerce/sim/create-client", // required
},
{
    j: 0, //index for day of the week. Monday at 1, Sunday at 7, 0 is everyday, null for specific date
    h: 16, // required, must be valid
    m: 30, // required, must be valid
    date: null, // Date format if j != null, else  null.
    name: "Creer un compte random", // required
    route: "/e-commerce/sim/create-client", // required
},
{
    j: 0, //index for day of the week. Monday at 1, Sunday at 7, 0 is everyday, null for specific date
    h: 18, // required, must be valid
    m: 30, // required, must be valid
    date: null, // Date format if j != null, else  null.
    name: "Creer un compte random", // required
    route: "/e-commerce/sim/create-client", // required
},
...res,
]