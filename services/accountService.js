const {Address} = require('../models/index');
const bcrypt = require('bcrypt');
const { default: Axios } = require('axios');
const logger = require('../logs');


async function createOne(account) {
    Axios.post(process.env.URL_RELATION_CLIENT + "/new-account", account).then().catch((e) => {
        logger.error('call relation client ' + e);
    });
    /*return Account.create({
        firstName: account.firstName,
        lastName: account.lastName,
        email: account.email,
        civility: account.civility,
        password: bcrypt.hashSync(account.password, 10)
    }).then((newaAccount) => {
        account.address.forEach((address) => {
            Address.create({
                ...address, 
                email: account.email
            }).then().catch();
        });
        return true;
    }).catch((e) => {
        return false;
    })*/
}

async function getAll() {
    try {
        return (await Axios.get(process.env.URL_RELATION_CLIENT + "/client-account-list")).data;
    } catch (e) {
        logger.error(e);
        return [];
    }
}

async function getAllNew(){
    const accounts = await Account.findAll({
        where: {
            isNew: true
        },
        include: [{
                model: Address,
                as: 'address'
        }]
    });
    accounts.forEach((account) => {
        account.isNew = false;
        account.save().then().catch();
    })
    return accounts;
}

/*async function login(account) {
    return Account.findOne({
        where: {
            email: account.email,
        }
    }).then((account) => {
        if (account) {

        } else {
            return false
        }
    }).catch(() => false);
}*/

module.exports = {
    createOne,
    getAll,
    getAllNew,
}