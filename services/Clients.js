const axios = require("axios");

function getClientList(next) {
    axios.get(process.env.URL_RELATION_CLIENT + '/clients').then((resp) => {
        if (resp.status === 200){
            next(resp.data);
        }
    }).catch((e) => {
        console.log(e);
        next([]);
    })
}

module.exports.getClientList = getClientList;