const prenoms = require('../models/exemple/prenoms.json');
const families = require('../models/exemple/family.json');
const address = require('../models/exemple/address.json');
const { default: Axios } = require('axios');
const logger = require('../logs');

function generateCreditCard(num) {
    let result = ''
    const key='0123456789';
    for (i in [...Array(num).keys()]) {
        result += key[Math.floor(Math.random() * key.length)];
    }
    return result;
}

function generateValidityDate() {
    const mounths = ['01','02','03','04','05','06','07','08','09','10','11','12'];
    const years = ['21','22','22','24','25','26','27','28','29','30'];
    return mounths[Math.floor(Math.random() * mounths.length)] + '/' + years[Math.floor(Math.random() * years.length)];
}

function generateProvider() {
    const provider = ['Visa', 'Mastercard', 'American Express', 'Maestro'];
    return provider[Math.floor(Math.random() * provider.length)];
}

function createClient() {
    const prenom_data = prenoms[Math.floor(prenoms.length * Math.random())].fields;
    const prenom = prenom_data.prenom;
    const civility = prenom_data.genre;
    const family = families[Math.floor(families.length * Math.random())].patronyme;
    const data = {
        "first-name": prenom,
        "last-name": family,
        "email": prenom + '.' + family + "@ursi.epita.fr",
        "password": prenom + family + 'azerty',
        "civility": civility,
        "address" : [address[Math.floor(address.length * Math.random())], address[Math.floor(address.length * Math.random())]],
        "card": {
            "ccv": generateCreditCard(3),
            "card-number": generateCreditCard(16),
            "validity-date" : generateValidityDate(),
            "provider" : generateProvider()
        }
    }
    Axios.post(process.env.URL_E_COMMERCE + '/account/create/one', data).then().catch((e) => {
        logger.error("simulator => " + e);
    });
    return data;
}

async function createOrder() {
    const clients = await(await Axios.get(process.env.URL_E_COMMERCE + '/account')).data;
    const products = await (await Axios.get(process.env.URL_E_COMMERCE + '/product')).data;
    let cart = [];
    let art_nb = Math.floor(Math.random() * 15) + 1;
    const client = clients[Math.floor(clients.length * Math.random())]
    while ( art_nb > 0) {
        const product = products[Math.floor(Math.random() * products.length)];
            const itemId = cart.findIndex((value) => value['product-code'] === product.product_code);
            if (itemId !== -1) {
                cart[itemId].quantity ++;
            } else {
                cart.push({
                    'product-code': product.product_code,
                    quantity: 1,
                })
            }
            art_nb --;
    }
    const data =  {
        account : client.account,
        products : cart,
        "delivery-address": client['address-list'][Math.floor(Math.random() * client['address-list'].length)], 
        "billing-address" : client['address-list'][Math.floor(Math.random() * client['address-list'].length)]
    }
    Axios.post(process.env.URL_E_COMMERCE + '/order/add', data).then().catch();
    return data;



}

module.exports.createClient = createClient;
module.exports.createOrder = createOrder;