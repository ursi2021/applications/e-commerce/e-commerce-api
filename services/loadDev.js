const models = require('../models');
const productsData = require('../models/exemple/products.json');
const orders = require('../models/exemple/orders.json');
const addressData = require('../models/exemple/address.json');
const payments = require('../models/exemple/payments.json');
const productOrders = require('../models/exemple/productOrder.json');
const accountsData = require('../models/exemple/account.json');

const ProductServices = require('./productService');
const PromotionServices = require('./promotionService');
const AccountServices = require('./accountService');

async function loadData(req, res, next) {
    for(const i in productsData) {
        await models.Product.upsert(productsData[i]);
    }
    if (res){
        res.status(200).json('All data has populated');
    }
}

function generateCreditCard(num) {
    let result = ''
    const key='0123456789';
    for (i in [...Array(num).keys()]) {
        result += key[Math.floor(Math.random() * key.length)];
    }
    return result;
}

function generateValidityDate() {
    const mounths = ['01','02','03','04','05','06','07','08','09','10','11','12'];
    const years = ['21','22','22','24','25','26','27','28','29','30'];
    return mounths[Math.floor(Math.random() * mounths.length)] + '/' + years[Math.floor(Math.random() * years.length)];
}

function genreateProvider() {
    const provider = ['Visa', 'Mastercard', 'American Express', 'Maestro'];
    return provider[Math.floor(Math.random() * provider.length)];
}

async function loadSenario() {
    const tryProduct = await ProductServices.updateProductList();
    const tryPromotion = await PromotionServices.updatePromotionList();
    if (!tryProduct){
        for(const i in productsData) {
            await models.Product.upsert(productsData[i]);
        }
    }
    for (const i in accountsData) {
        await models.Account.upsert(accountsData[i]);
    }
    for(const i in addressData) {
        await models.Address.findOne({
            where: {
                id: addressData[i].id
            }
        }).then(async (adress) => {
            if (!adress) {
                await models.Address.create(addressData[i]);
            }
        });
    }
    const accounts = await AccountServices.getAll();
    const products = await ProductServices.getProductList();
    for (i in [...Array(10 + Math.floor(Math.random() * 30)).keys()]) {
        let prodsize = 1 + Math.floor(Math.random() * 15);
        const cart = [];
        const user = accounts[Math.floor(Math.random() * accounts.length)];
        let sum_price = 0;
        while (prodsize > 0) {
            const product = products[Math.floor(Math.random() * products.length)];
            const itemId = cart.findIndex((value) => value.product_code === product.product_code);
            sum_price += product.price;
            if (itemId !== -1) {
                cart[itemId].quantity ++;
            } else {
                cart.push({
                    product_code: product.product_code,
                    quantity: 1,
                    price: product.price,
                })
            }
            prodsize --;
        }
        const delivery_address  = user.address[Math.floor(Math.random() * user.address.length)].dataValues;
        const billing_address  = user.address[Math.floor(Math.random() * user.address.length)].dataValues;
        const order = await models.Order.create({
            sum_price: sum_price,
            client_id: user.email,
            isSend: false,
            delivery_address_id: delivery_address.id,
            invoice_address_id: billing_address.id,
        });
        cart.forEach((value) => {
            models.ProductOrder.create({
                ...value,
                OrderId: order.id,
            })
        });
        models.Payment.create({
            amount: sum_price,
            card_number: generateCreditCard(16),
            first_name: user.firstName,
            last_name: user.lastName,
            cvc: generateCreditCard(3),
            validity_date: generateValidityDate(),
            provider: genreateProvider(),
            status: "pending",
            OrderId: order.id,
        })
    }
}

module.exports = {
    loadData,
    loadSenario,
};