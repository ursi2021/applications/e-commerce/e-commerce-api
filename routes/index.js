const express = require('express');
const router = express.Router();

const helloRoute = require('./helloworld');
const refRoute = require('./referenciel');
const orderRoute = require('./order');
const productRoute = require('./product');
const loadData = require('../services/loadDev');
const promotionRoute = require('./promotion');
const accountRoute = require('./account');
const clockRoute = require('./clock.route');
const simRoute = require('./simulator.route');

/* GET home page. */
/*router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});*/

router.use(helloRoute);
router.use(refRoute);
router.use(orderRoute);
router.use('/e-commerce/product', productRoute);
router.use('/e-commerce/promotion', promotionRoute);
router.use('/e-commerce', accountRoute);
router.use(clockRoute);
router.use('/e-commerce/sim', simRoute);

/* GET users listing. */
/**@swagger
 * /dev/load:
 *   get:
 *     tags:
 *       - dev
 *     summary: load developement data into databases
 *     responses:
 *       200:
 *         description: Successed processed.
 */
router.get('/dev/load', loadData.loadData );

router.get('/dev/senario', (req, res) => {
  loadData.loadSenario();
  res.status(200).body('Ok');
})

module.exports = router;
