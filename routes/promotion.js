const express = require('express');
const router = express.Router();
const promotionController = require('../controller/promotionControler');

/* GET users listing. */
/**@swagger
 * /e-commerce/promotion/load:
 *   get:
 *     tags:
 *       - product
 *     summary: Load promotion from 'gestion promotion'
 *     responses:
 *       200:
 *         description: Request accepted.
 *       500:
 *         description: Internal server error.
 */
router.get('/load', promotionController.updatePromotionList);
router.post('/load', promotionController.updatePromotionList);
router.get('/', promotionController.getPromotionList);

module.exports = router;