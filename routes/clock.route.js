const express = require('express');
const router = express.Router();
const clockparams = require('../services/clock.service');

router.get('/clock-register', (req, res, next) => {
    res.status(200).json(clockparams);
});

module.exports = router;