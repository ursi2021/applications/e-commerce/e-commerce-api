const express = require('express');
const router = express.Router();
const orderControler = require('../controller/orderControler');

/* GET users listing. */
/**@swagger
 * /e-commerce/web-sales:
 *   get:
 *     tags:
 *       - order
 *     summary: Return order list
 *     responses:
 *       200:
 *         description: Request accepted.
 *       404:
 *         description: No order was found.
 *       500:
 *         description: Internal server error.
 */
router.get('/e-commerce/web-sales', orderControler.getDayOrder);

/* GET users listing. */
/**@swagger
 * /e-commerce/payment:
 *   get:
 *     tags:
 *       - order
 *     summary: Return list of payment 
 *     responses:
 *       200:
 *         description: Return payment list.
 *       500:
 *         description: Internal server error.
 */
router.get('/e-commerce/payment', orderControler.getPayment);


/**@swagger
 * /e-commerce/order/add:
 *   post:
 *     tags:
 *       - order
 *     summary: Add new order 
 *     responses:
 *       200:
 *         description: Successefully saved.
 *       500:
 *         description: Internal server error.
 */
router.post('/e-commerce/order/add', orderControler.addOrder);


/**@swagger
 * /e-commerce/order/end-day:
 *   get:
 *     tags:
 *       - order
 *     summary: Launch process of compile order of day
 *     responses:
 *       200:
 *         description: Succesfully started
 */
router.get('/e-commerce/order/end-day', orderControler.endDay);
router.post('/e-commerce/order/end-day', orderControler.endDay);


router.get('/e-commerce/order/appview', orderControler.getOrderForApp);

router.get('/e-commerce/vieworder', orderControler.viewOrder);

module.exports = router;