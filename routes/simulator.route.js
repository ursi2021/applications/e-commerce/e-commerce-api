const express = require('express');
const router = express.Router();
const simController = require('../controller/simulator.controller');


router.get('/make-order',  simController.makerOrder);
router.get('/create-client', simController.createClient);
router.post('/make-order',  simController.makerOrder);
router.post('/create-client', simController.createClient);


module.exports = router;