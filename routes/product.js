const express = require('express');
const router = express.Router();
const productController = require('../controller/productController');

/* GET users listing. */
/**@swagger
 * /e-commerce/product/load:
 *   get:
 *     tags:
 *       - product
 *     summary: Load product from 'referenciel produit'
 *     responses:
 *       200:
 *         description: Request accepted.
 *       500:
 *         description: Internal server error.
 */
router.get('/load',  productController.updateProductList);
router.post('/load',  productController.updateProductList);
router.get('/', productController.getProductList);

module.exports = router;