var express = require('express');
const axios = require("axios");
var router = express.Router();
const accountControler = require('../controller/accountControler');


/**@swagger
 * /e-commerce/account/create/one:
 *   post:
 *     tags:
 *       - account
 *     summary: Create an account
 *     responses:
 *       200:
 *         description: Successfuly processed.
 *       409:
 *         description: Email already registered.
 */
router.post('/account/create/one', accountControler.createOneAccount);

/**@swagger
 * /e-commerce/account/create/multiple:
 *   post:
 *     tags:
 *       - account
 *     summary: Create an account
 *     responses:
 *       200:
 *         description: return list of precessed eleements.
 */
router.post('/account/create/multiple', accountControler.createMultipleAccount);

/**@swagger
 * /e-commerce/account/acount :
 *   get:
 *     tags:
 *       - account
 *     summary: get account
 *     responses:
 *       200:
 *         description: return list of account.
 */
router.get('/account/', accountControler.getAll);

/**@swagger
 * /e-commerce/new-account :
 *   get:
 *     tags:
 *       - account
 *     summary: get account
 *     responses:
 *       200:
 *         description: return list of account.
 */
router.get('/new-account', accountControler.getAllNew);

module.exports = router;