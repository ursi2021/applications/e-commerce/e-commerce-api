var express = require('express');
var router = express.Router();
const ClientControler = require('../controller/clientsControler');
//const ProductControler = require('../controller/productControler');

/* GET users listing. */
/**@swagger
 * /e-commerce/clients:
 *   get:
 *     tags:
 *       - referentiel
 *     summary: Return list of referentiel clients
 *     responses:
 *       200:
 *         description: Ref clients HTML list
 */
router.get('/e-commerce/clients', function(req, res, next) {
    ClientControler.clientsControler(req, res, next);
});


/**@swagger
 * /e-commerce/produits:
 *   get:
 *     tags:
 *       - referentiel
 *     summary: Return list of referentiel Products
 *     responses:
 *       200:
 *         description: Ref products HTML list
 */
/*router.get('/e-commerce/produits', function (req, res, next) {
    ProductControler.getProductList(req, res, next);
})*/
module.exports = router;