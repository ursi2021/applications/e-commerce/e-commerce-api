var express = require('express');
const axios = require("axios");
var router = express.Router();

/* GET users listing. */
/**@swagger
 * /e-commerce/hello:
 *   get:
 *     tags:
 *       - hello
 *     summary: Return hello world object
 *     responses:
 *       200:
 *         description: hello object
 */
router.get('/e-commerce/hello', function(req, res, next) {
  res.status(200).json({ 'e-commerce' : 'Hello World !'});
});

/**@swagger
 * /e-commerce/hello/all:
 *   get:
 *     tags:
 *       - hello
 *     summary: Get hello world of all MS
 *     responses:
 *       200:
 *         description: Return hello liste HTML
 */
router.get('/e-commerce/hello/all', async function(req, res, next) {
  const services = [
    { name: 'Relaction client',
      url: process.env.URL_RELATION_CLIENT },
    { name: 'Décisionnel',
      url: process.env.URL_DECISIONNEL },
    { name: 'Monétique et paiement',
      url: process.env.URL_MONETIQUE_PAIEMENT },
    { name: 'Gestion promotion',
      url: process.env.URL_GESTION_PROMOTION },
    { name: 'référentiel produit',
      url: process.env.URL_REFERENTIEL_PRODUIT },
    { name: 'Gestion commercial',
      url: process.env.URL_GESTION_COMMERCIALE },
    { name: 'Back Office Magasin',
      url: process.env.URL_BACK_OFFICE_MAGASIN },
    { name: 'Gestion entrepots',
      url: process.env.URL_ENTREPROTS },
    { name: 'Caisse',
      url: process.env.URL_CAISSE },
    { name: 'E commerce',
      url: process.env.URL_E_COMMERCE }
  ]
  var result = [];
  for (let i = 0; i < services.length; i++) {
    const startTime =  Date.now();
    await axios.get(services[i].url + '/hello'
    ).then(function (response) {
      // handle success
      result.push({ ...services[i], status: response.status, data: JSON.stringify(response.data), time: Date.now() - startTime });
    }).catch((err) => {
      if (err.response) {
        result.push({
          ...services[i],
          status: err.response.status,
          data: JSON.stringify(err.response.data),
          time: Date.now() - startTime
        });
      } else {
        result.push({ ...services[i], status: '000', data: 'No connection', time: Date.now() - startTime });
      }
    });
  }
  res.render('hello', { services: result });
});

module.exports = router;
